function processJson() {

    data = document.getElementById('jsonInput').value
    
    if (validateJson(data)) {
        obj = JSON.parse(data);
        buildSchedule(obj);
        document.getElementById('jsonInput').value = '';
    }else alert("invalid json");

}

function buildSchedule(myJson) {


    var days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
    var limit = "24:00:00";
    var i;
    var lastMode = "Eco"
    for (i = 0; i <= days.length; i++) {
        createDay(days[i]);
        var rawData = groupBy(myJson, days[i]);
        var data = sortObject(rawData);
        var keys = Object.keys(data);
        keys = keys.filter(function(n){ return n != "null" }); 
        var size = keys.length;
        if (keys.length == 0 ) {
            modifyBar(100, lastMode, days[i]);
            continue;
        }
        duration = convTime(keys[0]);
        modifyBar(duration, lastMode, days[i]);
        for (var j = 0; j < size - 1; j++) {

            duration = timeDiff(keys[j], keys[j + 1]);
            mode = data[keys[j]][0];
            modifyBar(duration, mode, days[i]);

        }
    
        lastMode = data[keys[size-1]][0];
        duration = timeDiff(keys[size-1], limit);
        modifyBar(duration, data[keys[size-1]][0], days[i]);
    }

}

function validateJson(str) {

    try {
        return (JSON.parse(str) && !!str);
    } catch (e) {
        return false;
    }
};

function sortObject(o) {
    return Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {});

};

function timeDiff(curr, next) {
    curr = new Date('1970-01-01T'+curr+'Z');
    next = new Date('1970-01-01T'+next+'Z');
    diff = (next - curr) / 1000;
    return diff/3600 * 4.16;
    // curr = moment(curr, 'HH:mm:ss');
    // next = moment(next, 'HH:mm:ss');
    // result = moment.duration(moment(next).diff(moment(curr)));;
    //return parseInt(result.asSeconds()) / 3600 * 4.16;
};

function convTime(curr) {
    curr = new Date('1970-01-01T'+curr+ 'Z').getTime() / 1000 ;
    //return moment.duration(curr).asSeconds() / 3600 * 4.16;
    return curr/3600 * 4.16;
}


function groupBy(xs, key) {

    return xs.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x.MODE);

        return rv;
    }, {});

};

function createDay(value) {
    day = document.createElement("div");
    day.className = "progress";
    day.id = value;
    console.log(day);
    document.getElementById("main").appendChild(day);
};

function modifyBar(end, color, classId) {

    board = document.createElement("div");
    board.className = "progress-bar " + color;
    board.role = "progressbar"
    board.style.cssText = "width:" + end + "% ;";
    document.getElementById(classId).appendChild(board);

}